import os

__author__ = 'andrei.sfat'
# Write all the prime numbers lower than a given number in a file, one number per line.

from set_2 import ex_2


def _clean_file():
    with open(os.getcwd() + '/test_files/ex_2.txt', 'w'):
        print 'deleting contents of ex_2.txt'


def write_of_all_prime_numbers_to_file(number):
    all_prime_numbers = ex_2.all_prime_numbers(number)
    # convert list of integers to strings to easier save the prime numbers
    string_prime_numbers = [str(prime_number) + '\n' for prime_number in all_prime_numbers]
    with open(os.getcwd() + "/test_files/ex_2.txt", 'w') as fout:
        fout.writelines(string_prime_numbers)


def main():
    _clean_file()
    number = int(raw_input('Please enter a number:'))
    write_of_all_prime_numbers_to_file(number)
    print 'finished writing the numbers in ex_2.txt'


if __name__ == '__main__':
    main()