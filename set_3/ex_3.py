import os
__author__ = 'andrei.sfat'
# Find all the words used only once in a file containing text.


def read_file():
    with open(os.getcwd() + '/test_files/ex_3.txt', 'r') as fin:
        return fin.read()


def main():
    text = read_file()
    set_of_words = set(text.lower().split(" "))
    print 'unique words %s' % set_of_words

if __name__ == '__main__':
    main()
