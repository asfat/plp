import os
import glob

__author__ = 'andrei.sfat'
# Write a program that reads a number of files containing sorted numbers
# (one number per line) and outputs a large file with all the numbers from all the files sorted.


def _clean_file():
    with open(os.getcwd() + '/test_files/ex_2.txt', 'w'):
        print 'deleting contents of ex_4-all.txt'


def read_files():
    files = glob.glob(os.getcwd() + '/test_files/ex_4-*.txt')
    return files


def read_numbers_from_files(files):
    numbers = []
    for f in files:
        with open(f, 'r') as fin:
            numbers_as_strings = fin.readlines()
            # clean up numbers
            numbers_as_strings = [line.rstrip('\n') for line in numbers_as_strings]
            print 'numbers_as_strings: %s' % numbers_as_strings
            if numbers_as_strings:
                numbers_from_file = [int(x) for x in numbers_as_strings]
                print 'numbers_from_file: %s' % numbers_from_file
                numbers.extend(numbers_from_file)
            print 'numbers: %s' % numbers
    return sorted(set(numbers))


def main():
    _clean_file()
    files = read_files()
    numbers = read_numbers_from_files(files)
    with open(os.getcwd() + '/test_files/ex_4_all.txt', 'w') as fout:
        numbers_as_strings = [str(x) + '\n' for x in numbers]
        fout.writelines(numbers_as_strings)

if __name__ == '__main__':
    main()