import re
import urllib2

__author__ = 'andrei.sfat'
# Extract all the links in a website on a given URL.


def extract_links(url):
    data = urllib2.urlopen(url).read()

    links = re.findall('''href=["'](.[^"']+)["']''', data, re.I)

    print 'found %s links' % len(links)

    return links


def main():
    url_to_be_parsed = raw_input('Please enter an url: ')
    links = extract_links(url_to_be_parsed)
    print 'links: %s' % links


if __name__ == '__main__':
    main()