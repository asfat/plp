import os

__author__ = 'andrei.sfat'
# Compute the sum of all the numbers in a file. The file contains a number on each line.


def perform_sum_from_file():
    with open(os.getcwd() + '/test_files/ex_1.txt', 'r') as fin:
        data = fin.readlines()
        result = 0
        for line in data:
            if line:
                result += int(line)
        # cleanup data
        data = [line.rstrip('\n') for line in data]
        return data, result


def main():
    res = perform_sum_from_file()
    print 'sum for %s = [%s]' % (res[0], res[1])

if __name__ == '__main__':
    main()