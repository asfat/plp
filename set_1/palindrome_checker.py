__author__ = 'andrei.sfat'


def check_palindrome(number):
    original_number = number
    reversed_number = 0
    while number > 0:
        reversed_number *= 10
        reversed_number += number % 10
        number /= 10
    return reversed_number == original_number


def main():
    input_as_string = raw_input('Please insert a number:')
    input_number = int(input_as_string)
    print_result = 'The number ' + input_as_string + ' is'
    print_result += ' not ' if not check_palindrome(input_number) else ' '
    print_result += 'a palindrome'

    print print_result


if __name__ == '__main__':
    main()