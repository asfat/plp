__author__ = 'andrei.sfat'

from palindrome_checker import check_palindrome


def main():
    input_as_string = raw_input('Please insert a number:')
    input_number = int(input_as_string)

    palindrome_numbers = [x for x in range(10)]
    for x in range(10, input_number):
        if check_palindrome(x):
            palindrome_numbers.append(x)

    print 'list of palindrome numbers=%s' % palindrome_numbers


if __name__ == '__main__':
    main()
