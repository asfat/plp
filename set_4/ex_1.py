from MyQueue import MyQueue

__author__ = 'asfat'


def main():
    q = MyQueue(maxsize=2)
    q.enqueue('dog')
    q.enqueue('cat')
    print 'checking size: %s' % str(q.size())
    print 'dequeing item: %s ' % q.dequeue()
    print 'dequeing item: %s ' % q.dequeue()
    print 'checking if queue is empty: %s' % q.is_empty()


if __name__ == '__main__':
    main()