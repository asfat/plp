from set_4.MyLogger import MyLogger

__author__ = 'asfat'


def main():
    my_logger = MyLogger('my-fancy-logger')
    my_logger.debug('This is a debug message')
    my_logger.error('This is an error message')
    my_logger.info('This is an info message')


if __name__ == '__main__':
    main()