__author__ = 'andrei.sfat'
# Implement a Queue.


class MyQueue:
    def __init__(self, maxsize=0):
        if maxsize == 0:
            self.items = []
            self.maxsize = -1
        else:
            self.items = [None] * maxsize
            self.maxsize = maxsize

    def is_empty(self):
        return self.items == []

    def enqueue(self, item):
        if not self.full():
            return self.items.insert(0, item)
        else:
            raise BaseException('Queue is full!')

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

    def full(self):
        if self.maxsize == -1:
            return False
        else:
            return self.maxsize == len([x for x in self.items if x is not None])