__author__ = 'andrei.sfat'


class Priority:
    FATAL = ('FATAL', 0)
    ERROR = ('ERROR', 1)
    WARN = ('WARN', 2)
    INFO = ('INFO', 3)
    DEBUG = ('DEBUG', 4)
    TRACE = ('TRACE', 5)

    def __init__(self, level_as_string):
        self.default_level = self.DEBUG
        self.level = self._retrieve_level(level_as_string)

    def _get_all_priorities(self):
        return [self.FATAL, self.ERROR, self.WARN, self.INFO, self.DEBUG, self.TRACE]

    def _retrieve_level(self, level_as_string):
        if level_as_string is None:
            return self.default_level

        level = level_as_string.upper()
        priorities = self._get_all_priorities()
        level_found = None
        for priority in priorities:
            if priority[0] == level:
                level_found = priority
                break

        return level_found if level_found is not None else self.default_level

    def is_greater_or_equal(self, level):
        return self.level[1] >= level[1]
