import os

__author__ = 'asfat'

from PropertyFileReader import PropertyFileReader


def main():
    config_file = os.getcwd() + '/config.properties'
    property_file_reader = PropertyFileReader(config_file)
    print property_file_reader.get_value('logging.level')
    property_file_reader.add_value({'logging.format': '%(asctime)s %(message)s'})
    print property_file_reader.get_value('logging.format')


if __name__ == '__main__':
    main()