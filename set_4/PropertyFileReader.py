__author__ = 'andrei.sfat'
# Design a configuration reader library API and implement it.
# The reader should read a configuration file from disk and expose its values through the API you choose.


class PropertyFileReader:
    def __init__(self, configuration_file):
        self.configuration_file = configuration_file
        self._properties = {}
        self._read_properties(self.configuration_file)

    def _read_properties(self, configuration_file):
        with open(configuration_file, 'r') as fin:
            for prop in fin.readlines():
                property_pair = prop.split("=")
                self._properties.update({property_pair[0]: property_pair[1].rstrip()})

    def get_value(self, property_key):
        return self._properties.get(property_key)

    def add_value(self, property_pair):
        self._properties.update(property_pair)
        update_properties = []
        for key, value in self._properties.items():
            property_pair_as_string = key + '=' + value + '\n'
            update_properties.append(property_pair_as_string)
        with open(self.configuration_file, 'w') as fout:
            fout.writelines(update_properties)