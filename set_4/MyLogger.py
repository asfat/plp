from datetime import datetime
import os
from set_4.Priority import Priority
from set_4.PropertyFileReader import PropertyFileReader

__author__ = 'andrei.sfat'
# Design a logging library API and implement it.


class MyLogger:
    def __init__(self, logger_name):
        self.property_file_reader = None
        self.logger_name = logger_name
        self._load_property_file()

    def _load_property_file(self):
        config_file = os.getcwd() + '/config.properties'
        self.property_file_reader = PropertyFileReader(config_file)
        self._load_configuration()
        self.priority = Priority(self.logging_level_as_string)

    def _load_configuration(self):
        self.logging_level_as_string = self.property_file_reader.get_value('logging.level')
        self.logging_filename = self.property_file_reader.get_value('logging.filename')
        self.format = '[%s] [%s] [%s] %s'

    def _log(self, level, message, throwable=None):
        with open(self.logging_filename, 'a') as myfile:
            myfile.write(self._message_format(level, message, throwable))

    def _message_format(self, level, message, throwable=None):
        message_formatted = \
            self.format % (str(datetime.now()), level[0], self.logger_name, message)
        if throwable:
            message_formatted += ' : ' + str(throwable)

        message_formatted += '\n'

        return message_formatted

    def trace(self, message, throwable=None):
        if self.priority.is_greater_or_equal(Priority.TRACE):
            self._log(Priority.TRACE, message, throwable)

    def debug(self, message, throwable=None):
        if self.priority.is_greater_or_equal(Priority.DEBUG):
            self._log(Priority.DEBUG, message, throwable)

    def info(self, message, throwable=None):
        if self.priority.is_greater_or_equal(Priority.INFO):
            self._log(Priority.INFO, message, throwable)

    def warn(self, message, throwable=None):
        if self.priority.is_greater_or_equal(Priority.WARN):
            self._log(Priority.WARN, message, throwable)

    def error(self, message, throwable=None):
        if self.priority.is_greater_or_equal(Priority.ERROR):
            self._log(Priority.ERROR, message, throwable)

