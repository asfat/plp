from datetime import datetime

__author__ = 'andrei.sfat'
# Write a decorator that can be used to measure the execution time of functions.
# It should print the duration on the console.


def millis(start_time, end_time):
    dt = end_time - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return ms


def elapsed_time(func):
    def new_func(*args):
        start = datetime.now()
        print 'starting measuring the execution time at %s' % str(start)
        func(*args)
        end = datetime.now()
        print 'finished measuring the exeuction time at %s' % str(end)
        print 'it took %s ms to run %s' % (str(millis(start, end)), func.__name__)

    return new_func


def my_func():
    my_sum = 0
    for i in range(10000000):
        my_sum += i
    print my_sum


def main():
    f = elapsed_time(my_func)
    f()

if __name__ == '__main__':
    main()

