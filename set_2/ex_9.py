__author__ = 'andrei.sfat'
# Write a function that searches for a value in a list of already sorted values.
from ex_8 import my_sort


def my_search(sequence, element):
    imin = 0
    imax = len(sequence)
    while imin <= imax:
        imid = (imin + imax) / 2
        if sequence[imid] == element:
            return imid
        elif sequence[imid] < element:
            imin = imid + 1
        else:
            imax = imid - 1
    return -1


def main():
    sequence = [4, 6, 2, 1, 8, 10]
    my_sort(sequence)
    print sequence
    print my_search(sequence, 2)


if __name__ == '__main__':
    main()