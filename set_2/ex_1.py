# coding=utf-8
__author__ = 'andrei.sfat'
# 1. Write a function that reads a number from the console and checks if it’s prime.


def is_prime_v1(number):
    if number < 2:
        return False
    for i in range(2, number):
        if not number % i:
            return False
    return True


def is_prime_v2(number):
    if number < 2:
        return False

    return all(number % i for i in xrange(2, number))


def main():
    number = int(raw_input('Please enter a number:'))
    print_result = 'The number ' + str(number) + ' is'
    print_result += ' not ' if not is_prime_v2(number) else ' '
    print_result += 'prime'
    print print_result


if __name__ == '__main__':
    main()
