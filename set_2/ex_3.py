__author__ = 'andrei.sfat'
# Write a function that finds the fibonacci sequence up to n elements iteratively and another one recursively.


def fib_iterative(number):
    if number <= 1:
        return number
    else:
        a, b = 0, 1
        for i in range(number):
            a, b = b, a + b
        return a


def fib_recursive(number):
    if number <= 1:
        return number
    else:
        return fib_recursive(number - 1) + fib_recursive(number - 2)


def fib_generator():
    a, b = 0, 1
    while 1:
        yield a
        a, b = b, a + b


def main():
    number = int(raw_input('Please enter a number:'))
    fibonacci_sequence_iterative = [fib_iterative(x) for x in range(number)]
    print 'fibonacci sequence iterative version %s' % fibonacci_sequence_iterative
    fibonacci_sequence_recursive = [fib_recursive(x) for x in range(number)]
    print 'fibonacci sequence recursive version %s' % fibonacci_sequence_recursive
    fibonacci_generator = fib_generator()
    fibonacci_sequence_generator = [fibonacci_generator.next() for x in range(number)]
    print 'fibonacci sequence generator version %s' % fibonacci_sequence_generator
if __name__ == '__main__':
    main()

