from datetime import datetime
from time import sleep
from ex_11 import millis
__author__ = 'andrei.sfat'
# Write a decorator that can be used to measure the total execution of the decorated functions


def sleepy_decorator(func):
    def new_func(arg):
        func(arg)
        sleep(4)
    return new_func


def elapsed_time_for_decorator(func):
    def new_func(arg):
        start = datetime.now()
        func(arg)
        end = datetime.now()
        print 'it took %s ms to run %s' % (str(millis(start, end)), elapsed_time_for_decorator.__name__)
    return new_func


def dummy_function(name):
    print 'Hello, %s' % name


def main():
    dummy_func = sleepy_decorator(dummy_function)
    elapsed_time_for_dec = elapsed_time_for_decorator(dummy_func)

    elapsed_time_for_dec('Andrei')

if __name__ == '__main__':
    main()