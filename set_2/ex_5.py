# coding=utf-8
__author__ = 'andrei.sfat'
# Write a function that emulates the “filter” functionality: it should take another function func and an iterable
# iterable as arguments and should return an iterable of those values in iterable that returned a truthy value
# when passed to func.


def my_filter(func, iterable):
    it = iter(iterable)
    final_iterable = []
    for element in it:
        if func(element):
            final_iterable.append(element)

    return final_iterable


def is_even(number):
    return number % 2 == 0


def main():
    sequence_of_numbers = [x for x in range(10)]
    print my_filter(is_even, sequence_of_numbers)

if __name__ == '__main__':
    main()