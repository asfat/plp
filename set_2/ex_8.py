__author__ = 'andrei.sfat'
# Write a function that sorts a list of values.


def my_sort(sequence):
    if not sequence:
        raise RuntimeError('Cannot sort on empty list')
    gap = len(sequence)
    shrink = 1.3
    while True:
        swapped = False
        gap = int(gap / shrink)
        if gap < 1:
            gap = 1
        for i in xrange(len(sequence) - gap):
            if sequence[i] > sequence[i + gap]:
                k = sequence[i]
                sequence[i] = sequence[i + gap]
                sequence[i + gap] = k
                swapped = True
        if not swapped and gap <= 1:
            break


def main():
    l = [0, 5, 2, 10, 1, 4]
    l2 = ['x', 'b', 'd', 'a']
    my_sort(l)
    my_sort(l2)
    print l
    print l2


if __name__ == '__main__':
    main()