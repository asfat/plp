# coding=utf-8
__author__ = 'andrei.sfat'
# Write a function that emulates the “map" functionality: it should take another function func and an iterable iterable
# as arguments and should return an iterable of the results of applying func over each value in iterable.


def my_map(func, iterable):
    it = iter(iterable)
    final_iterable = []
    for element in it:
        final_iterable.append(func(element))

    return final_iterable


def double_number(number):
    return number * 2


def main():
    sequence_of_numbers = [x for x in range(10)]
    print my_map(double_number, sequence_of_numbers)


if __name__ == '__main__':
    main()