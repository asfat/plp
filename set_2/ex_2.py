__author__ = 'andrei.sfat'
# 2. Write a function that finds all the prime numbers lower than a given number.

from ex_1 import is_prime_v1


def all_prime_numbers(number):
    if number < 2:
        return None
    else:
        prime_numbers = [2]
        for i in range(3, number):
            if is_prime_v1(i):
                prime_numbers.append(i)
        return prime_numbers


def main():
    number = int(raw_input('Please enter a number:'))
    result = all_prime_numbers(number)
    if not result:
        print 'number provided is < 2, so we cannot find any prime numbers for it'
    else:
        print 'list of prime numbers=%s' % result


if __name__ == '__main__':
    main()