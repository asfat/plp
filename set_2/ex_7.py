__author__ = 'andrei.sfat'
# Use map and filter and reduce to find the sum of all numbers n lower than a given value which satisfy the following
# constraint: n * n - 1 divisible by 3 (eg. 4 * 4 - 1 == 15 and 15 div with 3).

from ex_4 import my_map
from ex_5 import my_filter
from ex_6 import my_reduce


def filter_condition(x):
    return (x * x - 1) % 3 == 0


def filter_condition_with_tuple(x):
    return x[1] % 3 == 0


# w/o map
def main():
    number = int(raw_input('Please enter a number:'))
    l = my_filter(filter_condition, range(number))
    print l
    l1 = my_reduce((lambda n1, n2: n1 + n2), my_filter(filter_condition, range(number)), 0)
    print 'l1=%s' % l1


# using map and tuple
def main2():
    number = int(raw_input('Please enter a number:'))
    map_with_tuple = my_map((lambda x: (x, x * x - 1)), range(number))
    print my_reduce((lambda n1, n2: n1 + n2), my_map((lambda (x, y): x), my_filter(filter_condition_with_tuple, map_with_tuple)), 0)


if __name__ == '__main__':
    print 'w/o map'
    main()
    print 'using 2 maps'
    main2()

