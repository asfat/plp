from datetime import datetime

__author__ = 'andrei.sfat'
# Write a memoization decorator to speed up the recursive fibonacci function.
from ex_3 import fib_recursive
from ex_11 import elapsed_time
from ex_11 import millis


def memoize(f):
    memo = {}

    def helper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return helper


def main():
    print 'testing without memoization decorator'
    f = elapsed_time(fib_recursive)
    f(40)
    print 'testing with memoization decorator'
    start = datetime.now()
    f2 = memoize(fib_recursive)
    f2(40)
    end = datetime.now()
    print 'it took %s ms to run %s' % (str(millis(start, end)), f2.__name__)


if __name__ == '__main__':
    main()