# coding=utf-8
__author__ = 'andrei.sfat'
# Implement stateful function that starts from 0 and returns successive values each time it’s called
# without using global variables (eg. a() == 0; a() == 1; a() == 2).


def my_generator():
    a = 0
    while 1:
        yield a
        a += 1


def main():
    generator = my_generator()
    print generator.next()
    print generator.next()
    print generator.next()
    print generator.next()
    print generator.next()
    print generator.next()
    print generator.next()

if __name__ == '__main__':
    main()
