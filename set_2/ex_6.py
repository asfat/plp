# coding=utf-8
__author__ = 'andrei.sfat'
# Write a function that emulates the “reduce” functionality: it should take another function func, an iterable iterable
# and an initial value initializer as arguments. It should return the final result of repeatedly applying func over
# the previous result (or initializer for the first time) and the next element in iterable.


def my_reduce(func, iterable, initializer=None):
    it = iter(iterable)
    if initializer is not None:
        result = initializer
        for element in it:
            result = func(result, element)
    else:
        result = next(it)
        for element in it[1:]:
            result = func(result, element)
    return result


def main():
    sequence_of_numbers = [x for x in range(1, 5)]
    print my_reduce((lambda n1, n2: n1 * n2), sequence_of_numbers, 2)


if __name__ == '__main__':
    main()